package com.example.fourth.Dwaliro;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.AdapterView;

import java.util.ArrayList;
import java.util.Arrays;

public class FirstAid extends AppCompatActivity {

    ArrayAdapter<String> adapter;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_aid);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ListView listView = (ListView) findViewById(R.id.myList);

        ArrayList<String> arrayFirstAid = new ArrayList<>();
        arrayFirstAid.addAll(Arrays.asList(getResources().getStringArray(R.array.array_first_aid)));

        adapter = new ArrayAdapter<>(FirstAid.this, android.R.layout.simple_list_item_1, arrayFirstAid);
        listView.setAdapter(adapter);




        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

                if(position == 0) {
                    Intent intent = new Intent(view.getContext(), AbdominalPain.class);
                    startActivity(intent);
                }
                else if (position == 1) {
                    Intent intent = new Intent(view.getContext(), AnimalBites.class);
                    startActivity(intent);
                }
                else if (position == 2) {
                    Intent intent = new Intent(view.getContext(), Bees.class);
                    startActivity(intent);
                }
                else if (position == 3) {
                    Intent intent = new Intent(view.getContext(), blood.class);
                    startActivity(intent);
                }
                else if (position == 4) {
                    Intent intent = new Intent(view.getContext(), broken.class);
                    startActivity(intent);
                }
                else if (position == 5) {
                    Intent intent = new Intent(view.getContext(), bruises.class);
                    startActivity(intent);
                }
                else if (position == 6) {
                    Intent intent = new Intent(view.getContext(), burns.class);
                    startActivity(intent);
                }
                else if (position == 7) {
                    Intent intent = new Intent(view.getContext(), choking.class);
                    startActivity(intent);
                }
                else if (position == 8) {
                    Intent intent = new Intent(view.getContext(), cuts.class);
                    startActivity(intent);
                }
                else if (position == 9) {
                    Intent intent = new Intent(view.getContext(), diarrhea.class);
                    startActivity(intent);
                }
                else if (position == 10) {
                    Intent intent = new Intent(view.getContext(), dizziness.class);
                    startActivity(intent);
                }
                else if (position == 11) {
                    Intent intent = new Intent(view.getContext(), fever.class);
                    startActivity(intent);
                }
                else if (position == 12) {
                    Intent intent = new Intent(view.getContext(), heart.class);
                    startActivity(intent);
                }
                else if (position == 13) {
                    Intent intent = new Intent(view.getContext(), heat.class);
                    startActivity(intent);
                }
                else if (position == 14) {
                    Intent intent = new Intent(view.getContext(), muscle.class);
                    startActivity(intent);
                }
                else if (position == 15) {
                    Intent intent = new Intent(view.getContext(), nosebleed.class);
                    startActivity(intent);
                }
                else if (position == 16) {
                    Intent intent = new Intent(view.getContext(), rectal.class);
                    startActivity(intent);
                }
                else if (position == 17) {
                    Intent intent = new Intent(view.getContext(), sprain.class);
                    startActivity(intent);
                }
                else if (position == 18) {
                    Intent intent = new Intent(view.getContext(), testicles.class);
                    startActivity(intent);
                }



            }
        });

        }


    }


























/*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);

        MenuItem item = menu.findItem(R.id.menuSearch);
        SearchView searchView = (SearchView)item.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                adapter.getFilter().filter(newText);
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }*/




