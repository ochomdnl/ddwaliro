package com.example.fourth.Dwaliro;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


    }


    public void onClickFirstAid(View view){
        Intent intent = new Intent(this, FirstAid.class);
        startActivity(intent);
    }
    public void onClickFitness(View view){
        Intent intent = new Intent(this, Fitness.class);
        startActivity(intent);
    }
    public void onClickDiet(View view){
        Intent intent = new Intent(this, Diet.class);
        startActivity(intent);
    }
    public void onClickMyDoctor(View view){
        Intent intent = new Intent(this, Doctor.class);
        startActivity(intent);
    }

}
